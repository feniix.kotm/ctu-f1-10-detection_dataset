import numpy as np

def rectangle_intersection(a,b,segment_size):
    dx = min(a[1][0], b[1][0]) - max(a[0][0], b[0][0])
    dy = min(a[1][1], b[1][1]) - max(a[0][1], b[0][1])
    s = dx*dy/(segment_size[0]*segment_size[1])
    if (dx>=0) and (dy>=0):
        if s < 0.3:
            return 0.3
        elif s < 0.6:
            return 0.6
        else:
            return 1.0
    else:
        return 0.0


def make_frame_labels(frame, grid_size, bb_rectangle):
    '''
    Returns array of labels in percentage 
    '''
    labels = []
    size = frame.shape
    x = grid_size[1]
    y = grid_size[0]
    segment_size = (size[0]//y,size[1]//x)
    for j in np.arange(0,y-0.5,0.5):
        for i in np.arange(0,x-0.5,0.5):
            s_rectangle = ((int((j)*segment_size[0]),int((i)*segment_size[1])), (int((j+1)*segment_size[0]),int((i+1)*segment_size[1])))
            intersection = rectangle_intersection(s_rectangle,bb_rectangle,segment_size)
            labels.append(intersection)#/(segment_size[0]*segment_size[1]))
    return labels


if __name__ == '__main__':
    import argparse

    class Parser:
        def __init__(self):
            self.__parser = argparse.ArgumentParser(description='Export segments from frames.')
            self.__parser.add_argument('-filename', '-f', required = True, metavar='FRAME', 
                                help='Frame filename to process')
            self.__args = self.__parser.parse_args()

        def get_args(self):
            return self.__args

    parser = Parser()
    args = parser.get_args()
    frame = np.load(args.filename)
    print(make_frame_labels(frame, (4,8), ((150,256),(900,1100))))
