import os
import torch
import numpy as np

####################
# FIND GPU device
####################


def get_device(gpu=0):   # Manually specify gpu
    if torch.cuda.is_available():
        device = torch.device(gpu)
    else:
        device ='cpu'

    return device


def get_free_gpu():
    os.system('nvidia-smi -q -d Memory |grep -A4 GPU|grep Free >tmp')
    memory_available = [int(x.split()[2]) for x in open('tmp', 'r').readlines()]
    index = np.argmax(memory_available[:-1])  # Skip the 7th card --- it is reserved for evaluation!!!

    return index   # Returns index of the gpu with the most memory available