import numpy as np

def get_segments_from_frame(frame, grid_size, segment_id=None):
    '''
    Returns np.array of grid segments #(2,4)=21 segments, (4,8)=105 segments
    '''
    x = grid_size[1]
    y = grid_size[0]
    size = frame.shape
    segment_size = (size[1]//y,size[2]//x)
    segments = []
    for j in np.arange(0,y-0.5,0.5):
        for i in np.arange(0,x-0.5,0.5):
            segment = frame[0:, int((j)*segment_size[0]):int((j+1)*segment_size[0]),int((i)*segment_size[1]):int((i+1)*segment_size[1])]               
            segments.append(segment)
    if segment_id == None:
        x = np.stack([segments[i] for i in range(len(segments))] ,axis=0)
        #print(x.shape)
        return x
    else:
        return np.asarray(segments[segment_id])
