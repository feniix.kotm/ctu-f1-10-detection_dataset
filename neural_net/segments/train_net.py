import layers.conv as conv
import layers.linear as linear
import layers.feature_extractor as extractor
import tools.make_segments_from_frame 
import config

from torchvision.transforms import GaussianBlur
import torch
import torch.nn as nn


# Train net

class TrainNet(nn.Module):
    def __init__(self):
        super(TrainNet, self).__init__()
        self.layer2 = linear.FullyConnected(512,100,1)
        #self.sigmoid = nn.Sigmoid()


    def forward(self, x):
        out = self.layer2(x)
        #out = self.sigmoid(out)
        out = torch.flatten(out)
        return out

    def save_model(self, destination):
        torch.save(self.state_dict(), destination, _use_new_zipfile_serialization=False)
        return


    def load_model(self, model_path):
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.cuda()
        else:
            map_location = 'cpu'

        self.load_state_dict(torch.load(model_path, map_location=map_location))


# Fix net

class FixNet(nn.Module):
    def __init__(self):
        super(FixNet, self).__init__()
        self.blur = GaussianBlur((5,5), sigma=(0.1, 2.0))
        self.layer1 = extractor.ModelResNet18()


    def forward(self, x):
        #zpracovani segmentu neuronovou siti
        x = self.blur(x)
        features = self.layer1(x)
        features = torch.flatten(features,start_dim=1)
        return features

    def save_model(self, destination):
        torch.save(self.state_dict(), destination, _use_new_zipfile_serialization=False)
        return


    def load_model(self, model_path):
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.cuda()
        else:
            map_location = 'cpu'

        self.load_state_dict(torch.load(model_path, map_location=map_location))