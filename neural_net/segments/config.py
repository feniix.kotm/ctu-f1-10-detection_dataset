import os

grid_size = (4,8)

import torch
import tools.find_gpu

server = False
if server:
    # server
    device = tools.find_gpu.get_device(int(tools.find_gpu.get_free_gpu()))
else:
    # local
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
