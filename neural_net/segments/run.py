import main_net
import dataset as ds
import config
import torch
import os
import torch.utils.data as tdata
import cv2
import numpy as np

def make_mask(old_mask, net_out,grid_size, frame_size):
    mask_stat = np.zeros((2*grid_size[0],2*grid_size[1]))
    for i in range(2*grid_size[0]-1):
        for j in range(2*grid_size[1]-1):
            x = net_out[i*(2*grid_size[1]-1)+j]
            mask_stat[i,j] += x
            mask_stat[i+1,j] += x
            mask_stat[i,j+1] += x
            mask_stat[i+1,j+1] += x
    if np.all(old_mask) == None:
        mask = np.zeros((frame_size))
    else:
        mask = old_mask*0.7
        #mask = np.zeros((frame_size))
    mask_part_size = (frame_size[0]//(grid_size[0]*2),frame_size[1]//(grid_size[1]*2))
    for i in range(2*grid_size[0]):
        for j in range(2*grid_size[1]):
            #if mask_stat[i,j]/4 < 0.3:
            #    mask_stat[i,j] = 0
            mask[(i*mask_part_size[0]):((i+1)*mask_part_size[0]),(j*mask_part_size[1]):((j+1)*mask_part_size[1]),:] += abs(mask_stat[i,j]/4)
    mask = mask/(np.amax(mask))
    print(np.amax(mask))
    return mask


if __name__ == '__main__':
    video_path = os.path.join('../../dataset/dataset_source/2018_0111_233503_004.MP4')
    labels_path = os.path.join('../../dataset/dataset_source/2018_0111_233503_004_labels.npy')
    grid_size = config.grid_size
    video_dataset = ds.VideoDataset(video_path,grid_size,labels_path)

    num_workers = 1
    model_name = 'model/gpu_c2'
    device = config.device
    net = main_net.MainNet(model_name) 
    net.to(device)
    mask = None
    with torch.no_grad():
        loader = tdata.DataLoader(video_dataset, batch_size=1, shuffle=False)
        for i,batch in enumerate(loader):
            segments = torch.squeeze(batch['segments']).to(device)
            label = torch.squeeze(batch['labels']).numpy().astype('i4')
            frame = torch.squeeze(batch['frame']).numpy()
            frame = cv2.rectangle(frame,(label[0][0],label[0][1]),(label[1][0],label[1][1]),(255,0,0),5)
            out = net(segments)
            mask = (make_mask(mask, out,grid_size, frame.shape))
            frame = (frame*mask).astype('uint8')
            
            print(i, out)
            cv2.imshow('Video', cv2.resize(np.array(frame, dtype = np.uint8),(960,540)))
            print(i)
            cv2.waitKey(1)

