import numpy as np
import torch.utils.data as tdata
import tools.make_frame_labels
import config
import cv2
import os
import torch


class FrameDataset(tdata.Dataset):
    def __init__(self,parts_folder, grid_size):
        super().__init__()
        self.grid_size = grid_size
        self.segments_per_frame = (2*self.grid_size[0]-1)*(2*self.grid_size[1]-1)
        self.dataset_folder = os.path.join(parts_folder)
        self.dataset_parts = sorted(os.listdir(self.dataset_folder))
        self.dataset_part = []
        self.dataset_part_labels = []
        self.dataset_part_lens = []
        self.dataset_files = []
        self.dataset_labels = []
        self.image = None
        self.frame = None
        self.segment = None
        self.bb_rect = None
        self.labels = None
        self.label = None
        for part in self.dataset_parts:
            self.dataset_part.append(os.path.join(self.dataset_folder,part,'images'))
            self.dataset_part_labels.append(os.path.join(self.dataset_folder,part,'labels','labels.npy'))
            self.dataset_part_lens.append(len(sorted(os.listdir(os.path.join(self.dataset_folder,part,'images')))))

            part_labels = np.load(os.path.join(self.dataset_folder,part,'labels','labels.npy'))
            for file in sorted(os.listdir(os.path.join(self.dataset_folder,part,'images'))):
                label_id = int(file[:6])
                self.dataset_files.append(os.path.join(self.dataset_folder,part,'images',file))
                self.dataset_labels.append(part_labels[label_id])


    def __len__(self):
        return sum(self.dataset_part_lens)*self.segments_per_frame


    def __getitem__(self, i):
        self.image = cv2.imread(self.dataset_files[i//self.segments_per_frame])
        self.frame = np.asarray(self.image).astype('f4').transpose((2, 0, 1)) / 255
        self.segment = tools.make_segments_from_frame.get_segments_from_frame(self.frame, self.grid_size, i%self.segments_per_frame)
        self.bb_rect = ((self.dataset_labels[i//self.segments_per_frame][1],self.dataset_labels[i//self.segments_per_frame][2]),(self.dataset_labels[i//self.segments_per_frame][1]+self.dataset_labels[i//self.segments_per_frame][3],self.dataset_labels[i//self.segments_per_frame][2]+self.dataset_labels[i//self.segments_per_frame][4]))
        self.labels = tools.make_frame_labels.make_frame_labels(self.image, self.grid_size, self.bb_rect)
        self.label = self.labels[i%self.segments_per_frame]
        return {'labels': np.asarray(self.label).astype('f4'),
                'frames': np.asarray(self.segment).astype('f4'),
                'key': i}


class VideoDataset(tdata.Dataset):
    def __init__(self, video_path, grid_size, labels_path=None):
        super().__init__()
        self.grid_size = grid_size
        self.segments_per_frame = (2*self.grid_size[0]-1)*(2*self.grid_size[1]-1)

        self.capture = cv2.VideoCapture(video_path)
        while not self.capture.isOpened():
            pass

        self.frame_count = int(self.capture.get(cv2.CAP_PROP_FRAME_COUNT))
        print(self.frame_count)
        self.labels_path = labels_path
        self.dataset_labels = None
        if labels_path == None:
            self.dataset_labels = np.zeros(self.__len__())
        else:
            self.dataset_labels = np.load(os.path.join(labels_path))

        self.frame = None
        self.segments = None
        self.bb_rect = None
        self.labels = None
        self.label = None
        self.image = None


    def __len__(self):
        return self.frame_count


    def __getitem__(self,i):
        ret, frame = self.capture.read()
        if frame is None:
            print("Frame is None")
            return None
        self.image = frame
        #print(frame.shape)
        self.frame = frame.transpose((2, 0, 1)) / 255     
        self.segment = tools.make_segments_from_frame.get_segments_from_frame(self.frame, self.grid_size)
        if self.labels_path == None:
            self.labels = self.dataset_labels
            self.label = self.labels[i]
        else:
            self.bb_rect = ((self.dataset_labels[i][1],self.dataset_labels[i][2]),(self.dataset_labels[i][1]+self.dataset_labels[i][3],self.dataset_labels[i][2]+self.dataset_labels[i][4]))
            self.label = self.bb_rect

        return {'labels': np.asarray(self.label).astype('f4'),
                'segments': (self.segment).astype('f4'),
                'frame': self.image,
                'key': i}
