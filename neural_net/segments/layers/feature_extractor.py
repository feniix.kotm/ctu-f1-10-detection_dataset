#!/usr/bin/env python

import torch
import torch.nn as nn
from torchvision import models


class ModelResNet18(nn.Module):
    def __init__(self):
        super(ModelResNet18, self).__init__()
        self.base_model = models.resnet18(pretrained=True)
        self.base_layers = list(self.base_model.children())
        self.layer0 = nn.Sequential(*self.base_layers[:-1])

    def forward(self, x):
        with torch.no_grad():
            layer0 = self.layer0(x)
            out = layer0
        return out


class ModelSqueezeNet(nn.Module):
    def __init__(self):
        super(ModelSqueezeNet, self).__init__()
        self.base_model = models.squeezenet1_0(pretrained=True)
        self.base_layers = list(self.base_model.children())
        self.layer0 = nn.Sequential(*self.base_layers[:-1])

    def forward(self, x):
        with torch.no_grad():
            layer0 = self.layer0(x)
            out = layer0
        return out


class ModelMobileNet(nn.Module):
    def __init__(self):
        super(ModelMobileNet, self).__init__()
        self.base_model = models.mobilenet_v2(pretrained=True)
        self.base_layers = list(self.base_model.children())
        self.layer0 = nn.Sequential(*self.base_layers[:-1])

    def forward(self, x):
        with torch.no_grad():
            layer0 = self.layer0(x)
            out = layer0
        return out