#!/usr/bin/env python

import torch
import torch.nn as nn

class FullyConnected(nn.Module):
    def __init__(self, in_d, h_d, out_d):
        super(FullyConnected, self).__init__()
        self.layer = nn.Sequential(
                                    nn.Linear(in_d, h_d),
                                    nn.BatchNorm1d(h_d),
                                    nn.LeakyReLU(),
                                    nn.Linear(h_d, h_d),
                                    nn.BatchNorm1d(h_d),
                                    nn.LeakyReLU(),
                                    nn.Linear(h_d, out_d),
                                    nn.BatchNorm1d(out_d),
                                    nn.LeakyReLU()
                                    )
    
    def forward(self, x):
        out = self.layer(x)
        return (out)

    def save_model(self, destination):
        torch.save(self.state_dict(), destination, _use_new_zipfile_serialization=False)
        return

    def load_model(self, model_path):
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.cuda()
        else:
            map_location = 'cpu'

        self.load_state_dict(torch.load(model_path, map_location=map_location))
