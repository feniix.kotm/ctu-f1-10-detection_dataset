#!/usr/bin/env python

import torch
import torch.nn as nn

#konvolucni vrstva
class ConvNet(nn.Module):
    def __init__(self, in_c, out_c):
        super(ConvNet, self).__init__()
        self.layer = nn.Sequential(
                                    nn.Conv2d(in_c, out_c, kernel_size=3, stride=1, padding=0),
                                    nn.Conv2d(out_c, out_c, kernel_size=5, stride=1, padding=0),
                                    )
    
    def forward(self, x):
        out = self.layer(x)
        return torch.squeeze(torch.squeeze(out,-1), -1)


    def save_model(self, destination):
        torch.save(self.state_dict(), destination, _use_new_zipfile_serialization=False)
        return


    def load_model(self, model_path):
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.cuda()
        else:
            map_location = 'cpu'

        self.load_state_dict(torch.load(model_path, map_location=map_location))