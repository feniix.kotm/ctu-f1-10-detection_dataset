import numpy as np
import torch.utils.data as tdata
from torch import optim
import torch.nn as nn
import config
import os
import math
import train_net
import torch
import dataset as ds

from datetime import datetime

grid_size = config.grid_size

def train(train_model, fix_model, train_dataset, validation_dataset, device, model_name, num_workers, epochs, batch_size, learning_rate, best_model_loss):
    criterion = nn.L1Loss(reduction="sum")
    optimiser = optim.Adam(train_model.parameters(), lr=learning_rate)
    loader = tdata.DataLoader(train_dataset, batch_size=batch_size, shuffle=False, num_workers = num_workers)

    validation_criterion = nn.L1Loss(reduction="sum")
    validation_loader = tdata.DataLoader(validation_dataset, batch_size=batch_size, shuffle=False, num_workers = num_workers)
    if best_model_loss == None:
    	best_model_loss = math.inf

    frames = None
    labels = None

    for epoch in epochs:
        average_loss = 0
        #Train on train data
        time_start = datetime.now()
        for i,batch in enumerate(loader):
            #nacti batch
            frames = batch['frames'].to(device)
            labels = batch['labels'].to(device)
            #forward
            features = fix_model(frames)
            out = train_model(features)
            loss = criterion(out,labels)
            average_loss += float(loss)
            #backward
            optimiser.zero_grad()
            loss.backward()
            optimiser.step()
            time_now = datetime.now()
            print('Epoch {} in progress: [{}] {:.02f}% , Estimated time: {} min, Loss: {:.06f}       '.format(epoch+1, ((int(i/len(loader)*20)*'#')+((20-int(i/len(loader)*20))*' ')), i/len(loader)*100, (100-(i/len(loader)*100))/((i/len(loader)*100)/(time_now-time_start).total_seconds())//60 if (time_now-time_start).total_seconds() > 0 and i/len(loader)*100 > 0 else 'NaN', float(loss/batch_size)), end= ('' if (i == len(loader)-1) else '\r'))
            with open(model_name + '_train_e_{}.log'.format(epoch), 'a') as f:
                f.write(str(float(loss)/labels.size()[0]))
                f.write('\n')
        print("Epoch {}/{} done, loss: {}".format(epoch+1, epochs, average_loss/(len(loader)*batch_size)))
        with open(model_name + '_train.log', 'a') as f:
            f.write(str(float(average_loss/(len(loader)*batch_size))))
            f.write('\n')
        #Test on validation data
        model_loss = 0
        for i, batch in enumerate(validation_loader):
            print('Validating learned model: [{}] {:.02f}%'.format(int(i/len(validation_loader)*20)*'#'+((20-int(i/len(validation_loader)*20))*' '),i/len(validation_loader)*100),end= ('' if (i == len(validation_loader)) else '\r'))
            with torch.no_grad():
                #nacti batch
                frames = batch['frames'].to(device)
                labels = batch['labels'].to(device)
                #forward
                features = fix_model(frames)
                out = train_model(features)
                loss = criterion(out,labels)
                model_loss += float(loss)
        print('Validation loss: {}'.format(model_loss/(len(validation_loader)*batch_size)))
        with open(model_name + '_val.log', 'a') as f:
            f.write(str(float(model_loss/(len(validation_loader)*batch_size))))
            f.write('\n')
        if model_loss <= best_model_loss:
            print("Saving parameters.")
            best_model_loss = model_loss
            train_model.save_model(model_name)
            print("Model saved")
        else:
            train_model.load_model(model_name)
            learning_rate = learning_rate/2
            optimiser = optim.Adam(train_model.parameters(), lr=learning_rate)
        

if __name__ == '__main__':

    train_dataset = ds.FrameDataset('../../dataset/dataset_data/TRAIN/parts', grid_size)
    validation_dataset = ds.FrameDataset('../../dataset/dataset_data/TEST/parts', grid_size)

    device = config.device

    learning_rate = 0.001/2
    batch_size = 105#840
    epochs = range(0,20,1)
    num_workers = 4
    model_name = 'model/gpu_t2'
    #model_name = 'model/gpu_c'
    best_model_loss = None
    start_new = True
    if start_new:
        best_model_loss = None
    else:
        with open(model_name+'_val.log') as f:
            lines = f.readlines()
            last = lines[-1]
            best_model_loss = float(last)

    train_model = train_net.TrainNet()
    train_model.to(device)
    fix_model = train_net.FixNet()
    fix_model.to(device)
    if not (start_new):
        train_model.load_model(model_name)
    train(train_model, fix_model, train_dataset, validation_dataset, device, model_name, num_workers, epochs, batch_size, learning_rate, best_model_loss)

