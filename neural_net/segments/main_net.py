import layers.conv as conv
import layers.linear as linear
import layers.feature_extractor as extractor
import tools.make_segments_from_frame 
import config
import train_net 

from torchvision.transforms import GaussianBlur
import torch
import torch.nn as nn


# Main net

class MainNet(nn.Module):
    def __init__(self, linear_model):
        super(MainNet, self).__init__()
        #Resnet
        self.blur = GaussianBlur((5,5), sigma=(0.1, 2.0))
        self.layer1 = extractor.ModelResNet18()
        self.layer2 = train_net.TrainNet()
        self.layer2.load_model(linear_model)


    def forward(self, x):
        #zpracovani segmentu neuronovou siti
        x = self.blur(x)
        features = self.layer1(x)
        features = torch.flatten(features,start_dim=1)
        out = self.layer2(features)
        out = torch.flatten(out)
        return out

    def save_model(self, destination):
        torch.save(self.state_dict(), destination, _use_new_zipfile_serialization=False)
        return


    def load_model(self, model_path):
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.cuda()
        else:
            map_location = 'cpu'

        self.load_state_dict(torch.load(model_path, map_location=map_location))
