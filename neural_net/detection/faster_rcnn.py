import dataset as ds
import config
import torch
import os
import torch.utils.data as tdata
import FRCNN_net
import cv2
import numpy as np
import math

class Averager:
    def __init__(self):
        self.current_total = 0.0
        self.iterations = 0.0

    def send(self, value):
        self.current_total += value
        self.iterations += 1

    def value(self):
        if self.iterations == 0:
            return 0
        else:
            return 1.0 * self.current_total / self.iterations

    def reset(self):
        self.current_total = 0.0
        self.iterations = 0.0


def train(model, train_dataset, test_dataset, device, num_workers, epochs, batch_size, model_name, best_loss=None):
    loader = tdata.DataLoader(train_dataset, batch_size=batch_size, shuffle=False, num_workers = num_workers)

    test_loader = tdata.DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers = num_workers)
    
    params = [p for p in model.parameters() if p.requires_grad]
    optimiser = torch.optim.SGD(params, lr=0.005,
                                momentum=0.9, weight_decay=0.0005)
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimiser,
                                                   step_size=3,
                                                   gamma=0.1)
    if best_loss == None:
        best_loss = math.inf
    loss_hist = Averager()
    test_loss_hist = Averager()

    for epoch in epochs:
        loss_hist.reset()
        model.train()
        for i,batch in enumerate(loader):
            images = batch['images'].to(device)
            targets = batch['targets']
            expected = []
            for j in range(len(batch['images'])):
                expected.append(dict())
                for t in targets:
                    expected[j][t] = targets[t][j].to(device)
            images = list(image for image in images)
            targets = expected
            
            loss_dict = model.train_step(images, targets)
            losses = sum(loss for loss in loss_dict.values())
            loss_value = losses.item()
            loss_hist.send(loss_value)
            optimiser.zero_grad()
            losses.backward()
            optimiser.step()

            with open('model/'+model_name + '_dir/' + model_name + '_train_class_e_{}.log'.format(epoch), 'a') as f:
                f.write(str(float(loss_dict['loss_classifier'])))
                f.write('\n')
            with open('model/'+model_name + '_dir/' + model_name + '_train_box_reg_e_{}.log'.format(epoch), 'a') as f:
                f.write(str(float(loss_dict['loss_box_reg'])))
                f.write('\n')
            with open('model/'+model_name + '_dir/' + model_name + '_train_objectness_e_{}.log'.format(epoch), 'a') as f:
                f.write(str(float(loss_dict['loss_objectness'])))
                f.write('\n')
            with open('model/'+model_name + '_dir/' + model_name + '_train_rpn_box_reg_e_{}.log'.format(epoch), 'a') as f:
                f.write(str(float(loss_dict['loss_rpn_box_reg'])))
                f.write('\n')
            if (i+1)%1000 == 0:
                print('Batch {}/{} done, loss:{}'.format(i+1, len(loader),loss_value))
        with open('model/'+model_name + '_dir/' + model_name + '_train.log'.format(epoch), 'a') as f:
            f.write(str(float(loss_hist.value())))
            f.write('\n')
        test_loss_hist.reset()
        model.train()
        for i,batch in enumerate(test_loader):
            images = batch['images'].to(device)
            targets = batch['targets']
            expected = []
            for j in range(len(batch['images'])):
                expected.append(dict())
                for t in targets:
                    expected[j][t] = targets[t][j].to(device)
            images = list(image for image in images)
            targets = expected
            
            loss_dict = model.train_step(images, targets)
            losses = sum(loss for loss in loss_dict.values())
            loss_value = losses.item()
            test_loss_hist.send(loss_value)
        with open('model/'+model_name + '_dir/' + model_name + '_val.log'.format(epoch), 'a') as f:
            f.write(str(float(test_loss_hist.value())))
            f.write('\n')
        if best_loss > test_loss_hist.value():
            model.save_model('model/'+model_name)
            print('Model saved')
        else:
            model.load_model('model/'+model_name)
            if lr_scheduler is not None:
                lr_scheduler.step()
        print(model_name)
        print('Epoch {}/{} done, loss:{}'.format(epoch+1,len(epochs),loss_hist.value()))




if __name__ == '__main__':

    device = config.device

    train_dataset = ds.ImageDataset('../../dataset/dataset_data/TRAIN/parts')
    test_dataset = ds.ImageDataset('../../dataset/dataset_data/TEST/parts')


    num_classes = 2
    model_name = 'gpu_frcnn_vgg16'
    model = FRCNN_net.FRCNN_net4(num_classes)
    #model_name = 'gpu_frcnn_mobilenet'
    #model = FRCNN_net.FRCNN_net2(num_classes)
    #model_name = 'gpu_frcnn_resnet'
    #model = FRCNN_net.FRCNN_net1(num_classes)
    #model_name = 'gpu_frcnn_squeezenet'
    #model = FRCNN_net.FRCNN_net3(num_classes)
    model.to(device)
    print(model_name)
    batch_size = 2
    epochs = range(0,500,1)
    num_workers = 4

    start_new = True
    if not (start_new):
        model.load_model('model/'+model_name)
    train(model, train_dataset, test_dataset, device, num_workers, epochs, batch_size, model_name)

