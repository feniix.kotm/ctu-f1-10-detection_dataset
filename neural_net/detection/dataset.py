import numpy as np
import torch.utils.data as tdata
import config
import cv2
import os
import torch
import tools.bb_transform

class ImageDataset(tdata.Dataset):
    def __init__(self,parts_folder):
        super().__init__()
        print('Dataset init')
        self.dataset_folder = os.path.join(parts_folder)
        self.dataset_parts = sorted(os.listdir(self.dataset_folder))
        self.dataset_part = []
        self.dataset_part_labels = []
        self.dataset_part_lens = []
        self.dataset_files = []
        self.dataset_labels = []
        self.image = None
        self.frame = None
        self.bb_rect = None
        no_anotation = 0
        for part in self.dataset_parts:
            self.dataset_part.append(os.path.join(self.dataset_folder,part,'images'))
            self.dataset_part_labels.append(os.path.join(self.dataset_folder,part,'labels','labels.npy'))
            self.dataset_part_lens.append(len(sorted(os.listdir(os.path.join(self.dataset_folder,part,'images')))))

            part_labels = np.load(os.path.join(self.dataset_folder,part,'labels','labels.npy'))
            for file in sorted(os.listdir(os.path.join(self.dataset_folder,part,'images'))):
                label_id = int(file[:9])
                if part_labels[label_id][0] == False:
                    #print(part_labels[label_id])
                    no_anotation+=1
                else:
                    #print(part_labels[label_id])
                    self.dataset_files.append(os.path.join(self.dataset_folder,part,'images',file))
                    self.dataset_labels.append(part_labels[label_id])
        print('Dataset created with {} files, False anotation on {} files'.format(len(self.dataset_files),no_anotation))

    def __len__(self):
        return len(self.dataset_files)


    def __getitem__(self, i):
        self.image = cv2.imread(self.dataset_files[i])
        self.image = cv2.resize(self.image, (int(self.image.shape[1]*50/100),int(self.image.shape[0]*50/100)))
        self.frame = np.asarray(self.image).astype('f4').transpose((2, 0, 1)) / 255
        target = {}
        self.bb_rect = torch.as_tensor([tools.bb_transform.xywh_2_xyxy(int(self.dataset_labels[i][1]*50/100), int(self.dataset_labels[i][2]*50/100), int(self.dataset_labels[i][3]*50/100), int(self.dataset_labels[i][4]*50/100))],dtype=torch.float32)
        target['boxes'] = self.bb_rect
        target['labels'] = torch.ones((1,), dtype=torch.int64)
        target['image_id'] = torch.tensor([i])
        target['area'] = (self.bb_rect[:, 3] - self.bb_rect[:, 1]) * (self.bb_rect[:, 2] - self.bb_rect[:, 0])
        target['iscrowd'] = torch.zeros((1,),dtype=torch.int64)
        return {'images': np.asarray(self.frame),
                'frames': self.image,
                'targets': target
                }
        
