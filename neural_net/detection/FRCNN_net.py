import torchvision
import torch
import torch.nn as nn
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor

class FRCNN_net1(nn.Module):
    def __init__(self, num_classes):
        super(FRCNN_net1, self).__init__()
        self.num_classes = num_classes
        self.model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)
        self.in_features = self.model.roi_heads.box_predictor.cls_score.in_features
        self.model.roi_heads.box_predictor = FastRCNNPredictor(self.in_features, self.num_classes)

    
    def forward(self, x):
        out = self.model(x)
        return out

    def train_step(self, images, targets):
        out = self.model(images, targets)
        return out

    def save_model(self, destination):
        torch.save(self.state_dict(), destination, _use_new_zipfile_serialization=False)
        return


    def load_model(self, model_path):
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.cuda()
        else:
            map_location = 'cpu'

        self.load_state_dict(torch.load(model_path, map_location=map_location))



import torch
import torch.nn as nn
import torchvision
from torchvision.models.detection import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator

class FRCNN_net2(nn.Module):
    def __init__(self, num_classes):
        super(FRCNN_net2, self).__init__()
        self.num_classes = num_classes

        backbone = torchvision.models.mobilenet_v2(pretrained=True).features
        backbone.out_channels = 1280

        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                   aspect_ratios=((0.5, 1.0, 2.0),))

        roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                output_size=7,
                                                sampling_ratio=2)
        
        self.model = FasterRCNN(backbone,
                   num_classes=2,
                   rpn_anchor_generator=anchor_generator,
                   box_roi_pool=roi_pooler)

    def forward(self, x):
        out = self.model(x)
        return out

    def train_step(self, images, targets):
        out = self.model(images, targets)
        return out

    def save_model(self, destination):
        torch.save(self.state_dict(), destination, _use_new_zipfile_serialization=False)
        return


    def load_model(self, model_path):
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.cuda()
        else:
            map_location = 'cpu'

        self.load_state_dict(torch.load(model_path, map_location=map_location))


import torch
import torch.nn as nn
import torchvision
from torchvision.models.detection import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator

class FRCNN_net3(nn.Module):
    def __init__(self, num_classes):
        super(FRCNN_net3, self).__init__()
        self.num_classes = num_classes

        backbone = torchvision.models.squeezenet1_0(pretrained=True).features
        backbone.out_channels = 512

        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                   aspect_ratios=((0.5, 1.0, 2.0),))

        roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                output_size=7,
                                                sampling_ratio=2)
        
        self.model = FasterRCNN(backbone,
                   num_classes=2,
                   rpn_anchor_generator=anchor_generator,
                   box_roi_pool=roi_pooler)

    def forward(self, x):
        out = self.model(x)
        return out

    def train_step(self, images, targets):
        out = self.model(images, targets)
        return out

    def save_model(self, destination):
        torch.save(self.state_dict(), destination, _use_new_zipfile_serialization=False)
        return


    def load_model(self, model_path):
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.cuda()
        else:
            map_location = 'cpu'

        self.load_state_dict(torch.load(model_path, map_location=map_location))


import torch
import torch.nn as nn
import torchvision
from torchvision.models.detection import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator

class FRCNN_net4(nn.Module):
    def __init__(self, num_classes):
        super(FRCNN_net4, self).__init__()
        self.num_classes = num_classes

        backbone = torchvision.models.vgg16(pretrained=True).features
        backbone.out_channels = 512

        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                   aspect_ratios=((0.5, 1.0, 2.0),))

        roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                output_size=7,
                                                sampling_ratio=2)
        
        self.model = FasterRCNN(backbone,
                   num_classes=2,
                   rpn_anchor_generator=anchor_generator,
                   box_roi_pool=roi_pooler)

    def forward(self, x):
        out = self.model(x)
        return out

    def train_step(self, images, targets):
        out = self.model(images, targets)
        return out

    def save_model(self, destination):
        torch.save(self.state_dict(), destination, _use_new_zipfile_serialization=False)
        return


    def load_model(self, model_path):
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.cuda()
        else:
            map_location = 'cpu'

        self.load_state_dict(torch.load(model_path, map_location=map_location))
