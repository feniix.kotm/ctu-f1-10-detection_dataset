import FRCNN_net
import dataset as ds
import config
import torch
import os
import torch.utils.data as tdata
import cv2
import numpy as np
import torchvision
from datetime import datetime


import math
import sys
import time
from coco_utils import get_coco_api_from_dataset
from coco_eval import CocoEvaluator
import utils

def _get_iou_types(model):
    model_without_ddp = model
    if isinstance(model, torch.nn.parallel.DistributedDataParallel):
        model_without_ddp = model.module
    iou_types = ["bbox"]
    if isinstance(model_without_ddp, torchvision.models.detection.MaskRCNN):
        iou_types.append("segm")
    if isinstance(model_without_ddp, torchvision.models.detection.KeypointRCNN):
        iou_types.append("keypoints")
    return iou_types

def evaluate(model, data_loader, device):
    n_threads = torch.get_num_threads()
    # FIXME remove this and make paste_masks_in_image run on the GPU
    torch.set_num_threads(1)
    cpu_device = torch.device("cpu")
    model.eval()
    metric_logger = utils.MetricLogger(delimiter="  ")
    header = 'Test:'

    coco = get_coco_api_from_dataset(data_loader.dataset)
    iou_types = _get_iou_types(model)
    coco_evaluator = CocoEvaluator(coco, iou_types)

    for data in metric_logger.log_every(data_loader, 100, header):
        images = data['images']
        targets = data['targets']
        expected = []
        for j in range(len(data['images'])):
            expected.append(dict())
            for t in targets:
                expected[j][t] = targets[t][j].to(device)
        targets = expected
        images = list(img.to(device) for img in images)

        if torch.cuda.is_available():
            torch.cuda.synchronize()
        model_time = time.time()
        outputs = model(images)

        outputs = [{k: v.to(cpu_device) for k, v in t.items()} for t in outputs]
        model_time = time.time() - model_time

        res = {target["image_id"].item(): output for target, output in zip(targets, outputs)}
        evaluator_time = time.time()
        coco_evaluator.update(res)
        evaluator_time = time.time() - evaluator_time
        metric_logger.update(model_time=model_time, evaluator_time=evaluator_time)

    # gather the stats from all processes
    metric_logger.synchronize_between_processes()
    print("Averaged stats:", metric_logger)
    coco_evaluator.synchronize_between_processes()

    # accumulate predictions from all images
    coco_evaluator.accumulate()
    coco_evaluator.summarize()
    torch.set_num_threads(n_threads)
    return coco_evaluator

#model = FRCNN_net.FRCNN_net4(2)
#model.load_model('model/gpu_frcnn_vgg16_65_e')
#model_name = 'gpu_frcnn_vgg16_65_e'

#model = FRCNN_net.FRCNN_net3(2)
#model.load_model('model/gpu_frcnn_squeezenet_231_e')
#model_name = 'gpu_frcnn_squeezenet_231_e'

model = FRCNN_net.FRCNN_net2(2)
model.load_model('model/gpu_frcnn_mobilenet_127_e')
model_name = 'gpu_frcnn_mobilenet_127_e'

#model = FRCNN_net.FRCNN_net1(2)
#model.load_model('model/gpu_frcnn_resnet_104_e')
#model_name = 'gpu_frcnn_resnet_104_e'

evaluating = True
run_mode = 'live'
if run_mode == 'video':
    out = cv2.VideoWriter('model/out_'+model_name+'.mp4',cv2.VideoWriter_fourcc('M','J','P','G'), 30, (960,540))

images_path = os.path.join('../../dataset/dataset_data/VALIDATE/parts')
video_dataset = ds.ImageDataset(images_path)

num_workers = 1

device = config.device

model.to(device)

with torch.no_grad():
    loader = tdata.DataLoader(video_dataset, batch_size=1, shuffle=False)
    if evaluating:
        evaluated = evaluate(model, loader, device)
    for i,batch in enumerate(loader):
        image = batch['images'].to(device)
        target = batch['targets']
        target_box = target['boxes'][0][0].numpy()
        time1 = datetime.now()
        model.eval()
        prediction = model(image)
        prediction[0]['boxes'].to('cpu')
        time2 = datetime.now()
        print('Frame rate: {}'.format(1/(time2-time1).total_seconds()))
        image = torch.squeeze(batch['frames']).numpy()
        print('{}/{}'.format(i+1,len(loader)))
        if len(prediction[0]['boxes']) > 0:
            keep = [0]#torchvision.ops.nms(prediction[0]['boxes'],prediction[0]['scores'],0.3)
        else:
            keep = []
        for j in keep:
            box = prediction[0]['boxes'][j]
            box = box.to('cpu')
            box = box.numpy()
            print('Label:',prediction[0]['labels'][j].to('cpu'),'Score:',prediction[0]['scores'][j].to('cpu'),'Box:',box)
            image = cv2.rectangle(image,(int(box[0]),int(box[1])),(int(box[2]),int(box[3])),(0,0,255),5)
        image = cv2.rectangle(image,(int(target_box[0]),int(target_box[1])),(int(target_box[2]),int(target_box[3])),(255,0,0),3)
        if run_mode == 'live':
            cv2.imshow('Video', image)
            cv2.waitKey(1)
        elif run_mode == 'video':
            out.write(image)
            cv2.imshow('Video', image)
            cv2.waitKey(1)

    if run_mode == 'video':    
        out.release()
    
            
