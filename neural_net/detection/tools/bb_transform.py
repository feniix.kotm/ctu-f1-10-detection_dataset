def xywh_2_xyxy(x,y,w,h):
    return [x,y,x+w,y+h]

def xyxy_2_xywh(x,y,x1,y1):
    return [min(x,x1),min(y,y1),abs(x-x1),abs(y-y1)]