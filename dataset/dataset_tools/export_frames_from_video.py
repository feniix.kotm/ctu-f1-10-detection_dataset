import  cv2
import argparse
import os
import numpy as np

import json

class Parser:
    def __init__(self):
        self.__parser = argparse.ArgumentParser(description='Export dataset from video.')
        self.__parser.add_argument('-filename', '-f', required = True, metavar='VIDEO', 
                            help='Video filename to process')
        self.__parser.add_argument('-dataset_part', '-dp', default='TRAIN', choices=['TEST', 'TRAIN', 'VALIDATE'], metavar='DATASET_PART', 
                            help='TEST TRAIN or VALIDATE')
        self.__parser.add_argument('-labels', '-lf', default=None, metavar='LABELS', 
                            help='Labels filename')
        self.__args = self.__parser.parse_args()

    def get_args(self):
        return self.__args


def get_new_id():
    data = None
    with open(os.path.join('../dataset_data/dataset_log.json'), "r", encoding="utf-8") as f:
        data = json.load(f)
        last_id = data["last_id"]
        new_id = last_id+1
        data["last_id"] = new_id
        f.close()
    with open(os.path.join('../dataset_data/dataset_log.json'), "w", encoding="utf-8") as f:
        json.dump(data, f)
        f.close()
    return new_id


def add_video_to_sources(dataset_part, id, filename, mode='TRAIN'):
    data = None
    with open(os.path.join('../dataset_data/dataset_log.json'), 'r',  encoding="utf-8") as f:
        data = json.load(f)
        f.close()
    video_log = data['included_videos']
    if str(id) in video_log:
        print('Duplicit video ID')
        exit(101)
    video_log[str(id)] = filename
    data['sections'][mode].append(id)
    with open(os.path.join('../dataset_data/dataset_log.json'), 'w',  encoding="utf-8") as f:
        json.dump(data, f)
        f.close()


parser = Parser()
args = parser.get_args()
video_filename = args.filename
labels_filename = args.labels
dataset_part = args.dataset_part

new_id = get_new_id()
add_video_to_sources(dataset_part, new_id, video_filename, dataset_part)

video_source_folder = os.path.join('../dataset_source/')

videocap = cv2.VideoCapture(os.path.join(video_source_folder, video_filename))

if not (os.path.exists(os.path.join('../dataset_data/', dataset_part))):
    os.mkdir(os.path.join('../dataset_data/', dataset_part))
if not (os.path.exists(os.path.join('../dataset_data/', dataset_part,'parts/'))):
    os.mkdir(os.path.join('../dataset_data/', dataset_part,'parts/'))
if not(os.path.exists(os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id)))):
    os.mkdir(os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id)))
if not(os.path.exists(os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id),'images'))):
    os.mkdir(os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id),'images'))
if not(os.path.exists(os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id),'labels'))):
    os.mkdir(os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id),'labels'))


#export jednotlivych snimku
count = 0
success = True
while success:
    success, image = videocap.read()
    if success:
        f = os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id),'images','{:09d}.jpg'.format(count))
        cv2.imwrite(f,image)
        count+=1
        if count%100 == 0:
            print("[{}] Image exported.".format(count))
    if cv2.waitKey(10) == 27:
        break
print("Frames exported")

#nastaveni souboru s labely
if labels_filename == None:
    if not(os.path.exists(os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id),'labels','labels.npy'))):
        with open(os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id),'labels','labels.npy'), "wb") as f:
            data = [[-1] * 5 for i in range(count)]
            np.save(f,data)
            f.close()
else: 
    from shutil import copyfile
    copyfile(labels_filename, os.path.join('../dataset_data/', dataset_part,'parts/', str(new_id),'labels','labels.npy'))

print("Labels done")