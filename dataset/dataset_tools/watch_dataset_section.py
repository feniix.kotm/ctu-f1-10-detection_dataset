import cv2
import numpy as np
import argparse
import os, sys

class Parser:
    def __init__(self):
        self.__parser = argparse.ArgumentParser(description='Export dataset from video.')
        self.__parser.add_argument('-section', '-s', required = True, choices=['TEST', 'TRAIN', 'VALIDATE'], metavar='SECTION', 
                                    help='TEST, TRAIN or VALIDATE')
        self.__args = self.__parser.parse_args()

    def get_args(self):
        return self.__args

parser = Parser()
args = parser.get_args()
section = '../dataset_data/' + args.section

if os.path.exists(section):
    parts = os.listdir(os.path.join(section, 'parts'))
    files = []
    labels = []
    for part in parts:
        part_labels = np.load(os.path.join(section,'parts',part,'labels','labels.npy'))
        for file in sorted(os.listdir(os.path.join(section,'parts',part,'images'))):
            label_id = int(file[:9])
            files.append(os.path.join(section,'parts',part,'images',file))
            labels.append(part_labels[label_id])

    for label_id,file in enumerate(files):
        image = cv2.imread(file)
        label = labels[label_id]
        if label[0] == 1:
            image = cv2.rectangle(image,(label[1],label[2]),(label[1]+label[3],label[2]+label[4]),(0,255,0),5)
        print('{}/{}'.format(label_id+1,len(files)))
        cv2.imshow('Video', image)
        cv2.waitKey(1)