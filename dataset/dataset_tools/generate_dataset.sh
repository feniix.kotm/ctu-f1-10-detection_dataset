mkdir ../dataset_data
cp ../../dataset_log_template.json ../dataset_data/dataset_log.json
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/2018_0111_233503_004.MP4 -lf ../dataset_source/2018_0111_233503_004_labels.npy
python export_frames_from_video.py -dp TEST -f ../dataset_source/2018_0111_233806_005.MP4 -lf ../dataset_source/2018_0111_233806_005_labels.npy
python export_frames_from_video.py -dp VALIDATE -f ../dataset_source/2018_0111_233828_006.MP4 -lf ../dataset_source/2018_0111_233828_006_labels.npy
python export_frames_from_video.py -dp VALIDATE -f ../dataset_source/2018_0111_233857_008.MP4 -lf ../dataset_source/2018_0111_233857_008_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/2018_0111_233922_009.MP4 -lf ../dataset_source/2018_0111_233922_009_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/CTU_F1_10_2_cut1.mkv -lf ../dataset_source/CTU_F1_10_2_cut1_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/CTU_F1_10_2_cut2.mkv -lf ../dataset_source/CTU_F1_10_2_cut2_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/CTU_F1_10_2_cut3.mkv -lf ../dataset_source/CTU_F1_10_2_cut3_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/CTU_F1_10_2_cut4.mkv -lf ../dataset_source/CTU_F1_10_2_cut4_labels.npy
python export_frames_from_video.py -dp VALIDATE -f ../dataset_source/CTU_F1_10_2_cut5.mkv -lf ../dataset_source/CTU_F1_10_2_cut5_labels.npy
python export_frames_from_video.py -dp TEST -f ../dataset_source/CTU_F1_10_cut1.mkv -lf ../dataset_source/CTU_F1_10_cut1_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/2021_0329_104518_003.MP4 -lf ../dataset_source/2021_0329_104518_003_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/2021_0329_104818_004.MP4 -lf ../dataset_source/2021_0329_104818_004_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/2021_0329_105418_006.MP4 -lf ../dataset_source/2021_0329_105418_006_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/2021_0329_110142_009.MP4 -lf ../dataset_source/2021_0329_110142_009_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/2021_0329_110624_011.MP4 -lf ../dataset_source/2021_0329_110624_011_labels.npy
python export_frames_from_video.py -dp TRAIN -f ../dataset_source/2021_0329_110323_010.MP4 -lf ../dataset_source/2021_0329_110323_010_labels.npy