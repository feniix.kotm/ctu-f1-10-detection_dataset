import numpy as np
import argparse
import os, sys


class Parser:
    def __init__(self):
        self.__parser = argparse.ArgumentParser(description='Repairs broken labels file, like minus coords in bb.')
        self.__parser.add_argument('-labels_file', '-lf', required = True, metavar='LABELS_FILE', 
                            help='Labels file to repair')
        self.__args = self.__parser.parse_args()

    def get_args(self):
        return self.__args

if __name__ == '__main__':
    parser = Parser()
    args = parser.get_args()
    labels_file = args.labels_file
    counter = 0
    labels = np.load(labels_file)

    for c,i in enumerate(labels):
        print(i)
        if i[0] == 1:
            if i[1] < 0 or i[2] < 0 or i[3] < 0 or i[4] < 0:
                counter +=1
                labels[c] = [0]*5
            if (i[1] == 0 and i[2] == 0 and i[3] == 0) or (i[1] == 0 and i[2] == 0 and i[4] == 0) or (i[1] == 0 and i[3] == 0 and i[4] == 0) or (i[2] == 0 and i[3] == 0 and i[4] == 0):
                counter +=1
                labels[c] = [0]*5
    np.save(labels_file,labels)
    print('Broken labels count: {}'.format(counter))