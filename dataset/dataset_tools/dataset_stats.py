import os,sys
import numpy as np

TRAIN = os.path.join('../dataset_data/TRAIN/')
TEST = os.path.join('../dataset_data/TEST/')
VALIDATE = os.path.join('../dataset_data/VALIDATE/')

sections = [TRAIN, TEST, VALIDATE]

for section in sections:
    if os.path.exists(section):
        parts = os.listdir(os.path.join(section, 'parts'))
        positive = 0
        negative = 0
        no_anotation = 0
        for part in parts:
            if os.path.exists(os.path.join(section,'parts',part,'images')) and os.path.exists(os.path.join(section,'parts',part,'labels','labels.npy')):
                files = os.listdir(os.path.join(section,'parts',part,'images'))
                labels = np.load(os.path.join(section,'parts',part,'labels','labels.npy'))
                for i in labels:
                    if i[0] == 0:
                        negative+=1
                    elif i[0] == 1:
                        positive+=1
                    else:
                        no_anotation+=1
        print('Section {} contains (positive:negative - not anotated):   {}:{} - {}'.format(section,positive,negative,no_anotation))
