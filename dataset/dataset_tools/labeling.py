import pygame
import pygame.locals
import numpy as np
import argparse
import os, sys

class Labeler:
    def __init__(self, part_folder_path, labels_path, image_size):
        self.part_folder_path = part_folder_path
        if labels_path == None:
            self.labels_path = os.path.join(part_folder_path, 'labels','labels.npy')
        else:
            self.labels_path = labels_path
        self.image_size = image_size

        # zjistim kolik je snimku ve slozce
        self.file_paths = sorted(os.listdir(os.path.join(self.part_folder_path,'images')))
        self.image_count = len(self.file_paths)

        #pocatecni nastaveni 
        self.image_id = 0
        self.image = None
        self.labels = [[-1] * 5 for i in range(self.image_count)]

    #nacte snimek podle image_id
    def load_image(self):
        print("Loading next image... {}/{}".format(self.image_id, self.image_count))
        self.image = pygame.image.load(os.path.join(self.part_folder_path,'images',self.file_paths[self.image_id]))

    #nacte ulozene labely pokud existuji
    def load_labels(self):
        if os.path.isfile(os.path.join(self.labels_path)):
            self.labels = np.load(os.path.join(self.labels_path))
        else:
            print("Labels file was not found")

    #nastavi aktualni label
    def set_image_label(self, image_id, value, rect=[0,0,0,0]):
        self.labels[image_id] = [value]+rect

    #ulozi nastavene labely
    def save_labels(self):
        npy_labels = np.array(self.labels)
        np.save(self.labels_path, npy_labels)
        print("Labels saved, shape: {}".format(str(npy_labels.shape)))

###
class Parser:
    def __init__(self):
        self.__parser = argparse.ArgumentParser(description='Export dataset from video.')
        self.__parser.add_argument('-part_folder', '-if', required = True, metavar='PART_FOLDER', 
                            help='Folder with dataset part to label')
        self.__parser.add_argument('-labels_file', '-lf', default=None, metavar='LABELS_FILE', 
                            help='File with labels')
        self.__parser.add_argument('-image_size', '-is', default=(1920,1080), metavar='IMAGE_SIZE', 
                            help='Image size')
        self.__args = self.__parser.parse_args()

    def get_args(self):
        return self.__args


if __name__ == '__main__':
    parser = Parser()
    args = parser.get_args()
    part_folder = args.part_folder
    labels_file = args.labels_file
    image_size = args.image_size
    
    labeler = Labeler(part_folder, labels_file, image_size)

    labeler.load_image()
    labeler.load_labels()

    pygame.init()
    screen = pygame.display.set_mode(labeler.image_size)
    clock = pygame.time.Clock()

    #nastaveni rectanglu posle labelu
    if labeler.labels[labeler.image_id][0] == -1:
        mouse_button_up_coords = None
        mouse_button_down_coords = None
    else:
        mouse_button_up_coords = [labeler.labels[labeler.image_id][1], labeler.labels[labeler.image_id][2]]
        mouse_button_down_coords = [labeler.labels[labeler.image_id][1]+labeler.labels[labeler.image_id][3], labeler.labels[labeler.image_id][2]+labeler.labels[labeler.image_id][4]]


    labeler.image_id = 0

    running = True
    while running:
        #zpracovani eventu
        for event in pygame.event.get():
            #ukonceni aplikace
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.locals.KEYDOWN:
                #mezernik - potvrzeni
                if event.key == pygame.locals.K_SPACE:
                    #potvrzeni labelu True
                    if (mouse_button_down_coords is not None and mouse_button_up_coords is not None and mouse_button_down_coords != mouse_button_up_coords):
                        labeler.set_image_label(labeler.image_id, True, [
                            min(mouse_button_down_coords[0], mouse_button_up_coords[0]), 
                            min(mouse_button_down_coords[1], mouse_button_up_coords[1]), 
                            abs(mouse_button_up_coords[0]-mouse_button_down_coords[0]), 
                            abs(mouse_button_up_coords[1]-mouse_button_down_coords[1])
                        ])
                    #potvrzeni labelu False
                    else:
                        labeler.set_image_label(labeler.image_id, False)
                    #ulozeni labelu
                    labeler.save_labels()
                    labeler.image_id += 1
                    
                    #osetreni preteceni -> cykleni dokola
                    if labeler.image_id >= labeler.image_count:
                        labeler.image_id = labeler.image_id%labeler.image_count
                    
                    labeler.load_image()
                    #nastaveni rectanglu podle labelu
                    if labeler.labels[labeler.image_id][0] == -1:
                        mouse_button_up_coords = [labeler.labels[labeler.image_id-1][1], labeler.labels[labeler.image_id-1][2]]
                        mouse_button_down_coords = [labeler.labels[labeler.image_id-1][1]+labeler.labels[labeler.image_id-1][3], labeler.labels[labeler.image_id-1][2]+labeler.labels[labeler.image_id-1][4]]
                    else:
                        mouse_button_up_coords = [labeler.labels[labeler.image_id][1], labeler.labels[labeler.image_id][2]]
                        mouse_button_down_coords = [labeler.labels[labeler.image_id][1]+labeler.labels[labeler.image_id][3], labeler.labels[labeler.image_id-1][2]+labeler.labels[labeler.image_id-1][4]]
                
                
                elif event.key == pygame.locals.K_UP:
                    #potvrzeni labelu True
                    if (mouse_button_down_coords is not None and mouse_button_up_coords is not None and mouse_button_down_coords != mouse_button_up_coords):
                        for skip in range(10):
                            labeler.set_image_label(labeler.image_id+skip, True, [
		                        min(mouse_button_down_coords[0], mouse_button_up_coords[0]), 
		                        min(mouse_button_down_coords[1], mouse_button_up_coords[1]), 
		                        abs(mouse_button_up_coords[0]-mouse_button_down_coords[0]),
		                        abs(mouse_button_up_coords[1]-mouse_button_down_coords[1])
		                    ])
                    #potvrzeni labelu False
                    else:
                        for skip in range(10):
                            labeler.set_image_label(labeler.image_id+skip, False)
                    #ulozeni labelu
                    labeler.save_labels()
                    labeler.image_id += 10
                    
                    #osetreni preteceni -> cykleni dokola
                    if labeler.image_id >= labeler.image_count:
                        labeler.image_id = labeler.image_id%labeler.image_count
                    
                    labeler.load_image()
                    #nastaveni rectanglu podle labelu
                    if labeler.labels[labeler.image_id][0] == -1:
                        mouse_button_up_coords = [labeler.labels[labeler.image_id-1][1], labeler.labels[labeler.image_id-1][2]]
                        mouse_button_down_coords = [labeler.labels[labeler.image_id-1][1]+labeler.labels[labeler.image_id-1][3], labeler.labels[labeler.image_id-1][2]+labeler.labels[labeler.image_id-1][4]]
                    else:
                        mouse_button_up_coords = [labeler.labels[labeler.image_id][1], labeler.labels[labeler.image_id][2]]
                        mouse_button_down_coords = [labeler.labels[labeler.image_id][1]+labeler.labels[labeler.image_id][3], labeler.labels[labeler.image_id-1][2]+labeler.labels[labeler.image_id-1][4]]
                
                
                #sipka vlevo - posun doleva
                elif event.key == pygame.locals.K_LEFT:
                    labeler.image_id -= 1
                    #osetreni pretecni - cykleni dokola
                    if labeler.image_id < 0:
                        labeler.image_id += labeler.image_count

                    labeler.load_image()
                    #nastaveni rectanglu podle labelu
                    if labeler.labels[labeler.image_id][0] == -1:
                        mouse_button_up_coords = None
                        mouse_button_down_coords = None
                    else:
                        mouse_button_up_coords = [labeler.labels[labeler.image_id][1], labeler.labels[labeler.image_id][2]]
                        mouse_button_down_coords = [labeler.labels[labeler.image_id][1]+labeler.labels[labeler.image_id][3], labeler.labels[labeler.image_id][2]+labeler.labels[labeler.image_id][4]]
                
                #sipka vpravo - posun doprava
                elif event.key == pygame.locals.K_RIGHT:
                    labeler.image_id += 1
                    #osetreni preteceni - cykleni dokola
                    if labeler.image_id >= labeler.image_count:
                        labeler.image_id = labeler.image_id%labeler.image_count
                    
                    labeler.load_image()
                    #nastaveni rectanglu posle labelu
                    if labeler.labels[labeler.image_id][0] == -1:
                        mouse_button_up_coords = None
                        mouse_button_down_coords = None
                    else:
                        mouse_button_up_coords = [labeler.labels[labeler.image_id][1], labeler.labels[labeler.image_id][2]]
                        mouse_button_down_coords = [labeler.labels[labeler.image_id][1]+labeler.labels[labeler.image_id][3], labeler.labels[labeler.image_id][2]+labeler.labels[labeler.image_id][4]]
                
                #q - ukonceni programu
                elif event.key == pygame.locals.K_q:
                    labeler.save_labels()
                    running = False
                    break

                # l - nacteni labelu
                elif event.key == pygame.locals.K_l:
                    labeler.load_labels()

            # stisk mysi
            elif event.type == pygame.MOUSEBUTTONDOWN:
                #left click
                if event.button == 1:
                    mouse_button_down_coords = pygame.mouse.get_pos()
                    mouse_button_up_coords = None
                #right_click
                else:
                    mouse_button_up_coords = None
                    mouse_button_down_coords = None
            # uvolneni mysi
            elif event.type == pygame.MOUSEBUTTONUP:
                #left click
                if event.button == 1:
                    mouse_button_up_coords = pygame.mouse.get_pos()
        
        if not running:
            break
        
        #vykresleni obrazku
        screen.blit(labeler.image, (0, 0))

        #vykresleni rectanglu
        if mouse_button_down_coords is not None:
            mouse_pos = pygame.mouse.get_pos()
            if mouse_button_up_coords is None:
                pygame.draw.rect(screen, (0,0,255), pygame.Rect(
                                min(mouse_button_down_coords[0], mouse_pos[0]), 
                                min(mouse_button_down_coords[1], mouse_pos[1]), 
                                abs(mouse_pos[0]-mouse_button_down_coords[0]), 
                                abs(mouse_pos[1]-mouse_button_down_coords[1]), 
                                ), 3) 
            else:
                pygame.draw.rect(screen, (0,0,255), pygame.Rect(
                                min(mouse_button_down_coords[0], mouse_button_up_coords[0]), 
                                min(mouse_button_down_coords[1], mouse_button_up_coords[1]), 
                                abs(mouse_button_up_coords[0]-mouse_button_down_coords[0]), 
                                abs(mouse_button_up_coords[1]-mouse_button_down_coords[1]), 
                                ), 3) 

        # Flip the display
        pygame.display.flip()
        clock.tick(30)

    pygame.quit()
    labeler.save_labels()
