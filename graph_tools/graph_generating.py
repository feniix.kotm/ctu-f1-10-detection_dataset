import matplotlib.pyplot as plt
import numpy as np
import argparse

class Parser:
    def __init__(self):
        self.__parser = argparse.ArgumentParser(description='Export dataset from video.')
        self.__parser.add_argument('-filename', '-f', required=True, metavar='LOG', 
                            help='Log filename to process')
        self.__args = self.__parser.parse_args()

    def get_args(self):
        return self.__args

parser = Parser()
args = parser.get_args()       

with open(args.filename, 'r') as f:
    data = []
    idx = []
    suma = 0
    for i,line in enumerate(f.readlines()):
        idx.append(i)
        data.append(float(line))
        suma+=float(line)
    
    plt.plot(idx, data, 'g--', label='epoch loss progress')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.title('Epoch loss')
    plt.legend()
    plt.show()
