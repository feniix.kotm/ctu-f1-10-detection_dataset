import matplotlib.pyplot as plt
import numpy as np
import argparse

class Parser:
    def __init__(self):
        self.__parser = argparse.ArgumentParser(description='Export dataset from video.')
        self.__parser.add_argument('-filename', '-f', required=True, metavar='LOG', 
                            help='Log filename to process')
        self.__parser.add_argument('-epochs', '-e', required=True, metavar='Epochs', 
                            help='Epochs number')
        self.__args = self.__parser.parse_args()

    def get_args(self):
        return self.__args

parser = Parser()
args = parser.get_args()       

data = []
idx = []
for e in range(int(args.epochs)):
    with open(args.filename + '_e_' + str(e) + '.log', 'r') as f:
        for i,line in enumerate(f.readlines()):
            idx.append(len(idx)+1)
            data.append(float(line))
        
plt.plot(idx, data, 'g--')
plt.xlabel('Batch')
plt.ylabel('Loss')
plt.title('Batch loss')
plt.legend()
plt.show()
